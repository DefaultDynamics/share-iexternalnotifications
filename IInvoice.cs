﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CLExternalNotifications
{
   public interface IInvoice
   {
      string sentInvoiceNotification(string docBasePath, int orderId);
   }
}
