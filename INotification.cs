﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CLExternalNotifications
{
   public interface INotification
   {
      string sentSaleNotifation(string docBasePath, int orderId, string[] docList);
   }
}
